class Board
  attr_reader :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize (grid = Board.default_grid)
    @grid = grid
    @marks = [:s, :x]
  end

  def count
    count = 0
    @grid.flatten.each { |position| count += 1 if position == :s}
    count
  end

  def full?
    @grid.flatten.all? { |position| position != nil }
  end

  def [](position)
    row, column = position
    grid[row][column]
  end

  def []=(position, value)
    row, column = position
    grid[row][column] = value
  end

  def empty?(position = nil)
    if position != nil
      return grid[position.first][position.last] == nil
    else
      return count == 0
    end
  end

  def place_random_ship
    raise "Board is full" if full? == true
    until full?
      grid[rand(2)][rand(2)] = :s
      return count
    end
  end

  def won?
    @grid.flatten.all? { |position| position != :s } ? true : false
  end

end
